using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace DeepBridge.Controllers
{
    public abstract class DeepBridgeControllerBase: AbpController
    {
        protected DeepBridgeControllerBase()
        {
            LocalizationSourceName = DeepBridgeConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
