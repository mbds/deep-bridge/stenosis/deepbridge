﻿using Abp.Domain.Entities;

namespace DeepBridge.Personnes
{
    public class Doctor : Entity<int>
    {
        public string Matricule { get; set; }
        public int UtilisateurId { get; set; }

        // Proprietée de navigation
        public virtual Utilisateur Utilisateur { get; set; }
    }
}
