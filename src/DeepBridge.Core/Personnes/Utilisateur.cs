﻿using Abp.Domain.Entities;
using DeepBridge.Predictions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DeepBridge.Personnes
{
    public class Utilisateur : Entity<int>
    {
        public Utilisateur()
        {
            PredictionDatas = new HashSet<PredictionData>();
        }
        public string Login { get; set; }
        public string CryptedMdp { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }

        // Proprietée de navigation
        [NotMapped]
        public virtual ICollection<PredictionData> PredictionDatas { get; set; }
        public virtual Admin Admin { get; set; }
        public virtual Doctor Doctor{ get; set; }
    }
}
