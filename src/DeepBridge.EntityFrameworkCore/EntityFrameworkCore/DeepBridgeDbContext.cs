﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using DeepBridge.Authorization.Roles;
using DeepBridge.Authorization.Users;
using DeepBridge.MultiTenancy;
using DeepBridge.Personnes;
using DeepBridge.Predictions;
using DeepBridge.Segments;

namespace DeepBridge.EntityFrameworkCore
{
    public class DeepBridgeDbContext : AbpZeroDbContext<Tenant, Role, User, DeepBridgeDbContext>
    {
        /* Define a DbSet for each entity of the application */

        public virtual DbSet<Admin> Admins { get; set; }
        public virtual DbSet<Doctor> Doctors { get; set; }
        public virtual DbSet<Utilisateur> Utilisateurs { get; set; }
        public virtual DbSet<CasEtude> CasEtudes { get; set; }
        public virtual DbSet<Model> Models { get; set; }
        public virtual DbSet<Prediction> Predictions { get; set; }
        public virtual DbSet<PredictionData> PredictionDatas { get; set; }
        public virtual DbSet<TrainModel> TrainModels { get; set; }
        public virtual DbSet<TypeClassification> TypeClassifications { get; set; }
        public virtual DbSet<TypeData> TypeDatas { get; set; }
        public virtual DbSet<TypeSegmentation> TypeSegmentations { get; set; }


        public DeepBridgeDbContext(DbContextOptions<DeepBridgeDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Personnes entités
            #region Personnes entities
            modelBuilder.Entity<Admin>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("Admin");

                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd();

                entity.Property(e => e.Email)
                      .HasColumnName("Email")
                      .HasMaxLength(100)
                      .IsRequired();

                // ForeignKeys
                #region ForeignKeys
                entity.HasOne(e => e.Utilisateur)
                      .WithOne(u => u.Admin)
                      .HasForeignKey<Admin>(d => d.UtilisateurId)
                      .OnDelete(DeleteBehavior.ClientSetNull)
                      .HasConstraintName("FK_Admin__Utilisateur");
                #endregion

            });

            modelBuilder.Entity<Doctor>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("Doctor");

                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd();

                entity.Property(e => e.Matricule)
                      .HasColumnName("Matricule")
                      .HasMaxLength(255)
                      .IsRequired();

                #region ForeignKeys
                entity.HasOne(e => e.Utilisateur)
                      .WithOne(u => u.Doctor)
                      .HasForeignKey<Doctor>(d => d.UtilisateurId)
                      .OnDelete(DeleteBehavior.ClientSetNull)
                      .HasConstraintName("FK_Doctor__Utilisateur");
                #endregion

            });

            modelBuilder.Entity<Utilisateur>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("Utilisateur");

                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd();

                entity.Property(e => e.Login)
                      .HasColumnName("Login")
                      .HasMaxLength(100)
                      .IsRequired();

                entity.Property(e => e.CryptedMdp)
                      .HasColumnName("CryptedMdp")
                      .HasMaxLength(255)
                      .IsRequired();

                entity.Property(e => e.Nom)
                      .HasColumnName("Nom")
                      .HasMaxLength(100)
                      .IsRequired();

                entity.Property(e => e.Prenom)
                      .HasColumnName("Prenom")
                      .HasMaxLength(100)
                      .IsRequired();

            });
            #endregion

            // Predictions entités
            #region Predictions

            modelBuilder.Entity<CasEtude>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("CasEtude");

                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd();

                entity.Property(e => e.LibelleCas)
                      .HasColumnName("LibelleCas")
                      .HasMaxLength(100)
                      .IsRequired();

                entity.Property(e => e.Description)
                      .HasColumnName("Description")
                      .HasMaxLength(255);

                entity.Property(e => e.Link)
                      .HasColumnName("Link")
                      .HasMaxLength(255);

                // ForeignKeys
                #region ForeignKeys
                entity.HasOne(e => e.TrainModel)
                      .WithOne(u => u.CasEtude)
                      .HasForeignKey<CasEtude>(d => d.TrainModelId)
                      .HasConstraintName("FK_CasEtude__TrainModel");

                entity.HasOne(e => e.Model)
                      .WithOne(u => u.CasEtude)
                      .HasForeignKey<CasEtude>(d => d.ModelId)
                      .HasConstraintName("FK_CasEtude__Model");

                entity.HasOne(e => e.PredictionData)
                      .WithOne(u => u.CasEtude)
                      .HasForeignKey<CasEtude>(d => d.PredictionDataId)
                      .HasConstraintName("FK_CasEtude__PredictionData");

                entity.HasOne(e => e.Admin)
                      .WithMany(u => u.CasEtudes)
                      .HasForeignKey(c => c.AdminId)
                      .HasConstraintName("FK_CasEtude__Admin");
                #endregion

            });

            modelBuilder.Entity<Model>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("Model");

                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd();

                entity.Property(e => e.Link)
                      .HasColumnName("Link")
                      .HasMaxLength(255);

                entity.Property(e => e.Task)
                      .HasColumnName("Task")
                      .HasMaxLength(100);

                entity.Property(e => e.ImResize)
                      .HasColumnName("ImResize");

                entity.Property(e => e.Aug)
                      .HasColumnName("Aug");

                entity.Property(e => e.Normalize)
                      .HasColumnName("Normalize");

                entity.Property(e => e.Batch)
                      .HasColumnName("Batch");

                entity.Property(e => e.Epoch)
                      .HasColumnName("Epoch");

                entity.Property(e => e.Rate)
                      .HasColumnName("Rate");

                entity.Property(e => e.Optimizeur)
                      .HasColumnName("Optimizeur")
                      .HasMaxLength(100);

                // ForeignKeys
                #region ForeignKeys
                entity.HasOne(e => e.CasEtude)
                      .WithOne(u => u.Model)
                      .HasForeignKey<Model>(d => d.CasEtudeId)
                      .HasConstraintName("FK_Model__CasEtude");

                entity.HasOne(e => e.TypeData)
                      .WithMany(u => u.Models)
                      .HasForeignKey(d => d.TypeDataId)
                      .HasConstraintName("FK_Model__TypeData");

                entity.HasOne(e => e.TypeSegmentation)
                      .WithMany(u => u.Models)
                      .HasForeignKey(d => d.TypeSegmentationId)
                      .HasConstraintName("FK_Model__TypeSegmentation");

                entity.HasOne(e => e.TypeClassification)
                      .WithMany(u => u.Models)
                      .HasForeignKey(d => d.TypeClassificationId)
                      .HasConstraintName("FK_Model__TypeClassification");
                #endregion

            });

            modelBuilder.Entity<Prediction>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("Prediction");

                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd();

                entity.Property(e => e.Link)
                      .HasColumnName("Link")
                      .HasMaxLength(255);

                entity.Property(e => e.Type)
                      .HasColumnName("Type")
                      .HasMaxLength(100)
                      .IsRequired();

                // ForeignKeys
                #region ForeignKeys
                entity.HasOne(e => e.PredictionData)
                      .WithOne(u => u.Prediction)
                      .HasForeignKey<Prediction>(d => d.PredictionDataId)
                      .HasConstraintName("FK_Prediction__PredictionData");
                #endregion

            });

            modelBuilder.Entity<PredictionData>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("PredictionData");

                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd();

                entity.Property(e => e.Libelle)
                      .HasColumnName("Libelle")
                      .HasMaxLength(100)
                      .IsRequired();

                entity.Property(e => e.Link)
                      .HasColumnName("Link")
                      .HasMaxLength(255);

                // ForeignKeys
                #region ForeignKeys
                entity.HasOne(e => e.CasEtude)
                      .WithOne(u => u.PredictionData)
                      .HasForeignKey<PredictionData>(d => d.CasEtudeId)
                      .HasConstraintName("FK_PredictionData__CasEtude");

                entity.HasOne(e => e.Prediction)
                      .WithOne(u => u.PredictionData)
                      .HasForeignKey<PredictionData>(d => d.PredictionId)
                      .HasConstraintName("FK_PredictionData__Prediction");

                entity.HasOne(e => e.Utilisateur)
                      .WithMany(u => u.PredictionDatas)
                      .HasForeignKey(d => d.UtilisateurId)
                      .HasConstraintName("FK_PredictionData__Utilisateur");
                #endregion

            });

            modelBuilder.Entity<TrainModel>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("TrainModel");

                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd();

                entity.Property(e => e.Libelle)
                      .HasColumnName("Libelle")
                      .HasMaxLength(100)
                      .IsRequired();

                entity.Property(e => e.Link)
                      .HasColumnName("Link")
                      .HasMaxLength(255);

                // ForeignKeys
                #region ForeignKeys
                entity.HasOne(e => e.CasEtude)
                      .WithOne(u => u.TrainModel)
                      .HasForeignKey<TrainModel>(d => d.CasEtudeId)
                      .HasConstraintName("FK_TrainModel__CasEtude");
                #endregion

            });

            #endregion

            // Segments entités
            #region Segments

            modelBuilder.Entity<TypeClassification>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("TypeClassification");

                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd();

                entity.Property(e => e.Libelle)
                      .HasColumnName("Libelle")
                      .HasMaxLength(100)
                      .IsRequired();

                entity.Property(e => e.Description)
                      .HasColumnName("Description")
                      .HasMaxLength(255);

            });

            modelBuilder.Entity<TypeData>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("TypeData");

                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd();

                entity.Property(e => e.Libelle)
                      .HasColumnName("Libelle")
                      .HasMaxLength(100)
                      .IsRequired();

                entity.Property(e => e.Description)
                      .HasColumnName("Description")
                      .HasMaxLength(255);

            });

            modelBuilder.Entity<TypeSegmentation>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("TypeSegmentation");

                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd();

                entity.Property(e => e.Libelle)
                      .HasColumnName("Libelle")
                      .HasMaxLength(100)
                      .IsRequired();

                entity.Property(e => e.Description)
                      .HasColumnName("Description")
                      .HasMaxLength(255);

            });
            #endregion


            // Needed to register global query filters
            base.OnModelCreating(modelBuilder);
        }
    }
}
