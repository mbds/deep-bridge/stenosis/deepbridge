﻿using Abp.AutoMapper;
using DeepBridge.Sessions.Dto;

namespace DeepBridge.Web.Views.Shared.Components.TenantChange
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}
