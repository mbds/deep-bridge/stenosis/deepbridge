using System.Collections.Generic;
using DeepBridge.Roles.Dto;

namespace DeepBridge.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}
