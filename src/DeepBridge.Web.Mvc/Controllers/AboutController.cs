﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using DeepBridge.Controllers;

namespace DeepBridge.Web.Controllers
{
    [AbpMvcAuthorize]
    public class AboutController : DeepBridgeControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}
