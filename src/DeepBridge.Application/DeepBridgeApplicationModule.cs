﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using DeepBridge.Authorization;

namespace DeepBridge
{
    [DependsOn(
        typeof(DeepBridgeCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class DeepBridgeApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<DeepBridgeAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(DeepBridgeApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
