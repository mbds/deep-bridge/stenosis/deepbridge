﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace DeepBridge
{
    public abstract class DeepBridgeAsyncCrudAppServiceBase<TEntity, TEntityDto, TListEntityDto, TSelectEntityDto, TPrimaryKey, TGetInput, TGetAllInput, TCreateInput, TUpdateInput, TDeleteInput>
       : CrudAppServiceBase<TEntity, TEntityDto, TPrimaryKey, TGetAllInput, TCreateInput, TUpdateInput>, IDeepBridgeAsyncCrudAppServiceBase<TEntityDto, TListEntityDto, TSelectEntityDto, TPrimaryKey, TGetInput, TGetAllInput, TCreateInput, TUpdateInput, TDeleteInput> where TEntity : class, IEntity<TPrimaryKey>
       where TEntityDto : IEntityDto<TPrimaryKey>
       where TUpdateInput : IEntityDto<TPrimaryKey>
       where TGetInput : IEntityDto<TPrimaryKey>
       where TDeleteInput : IEntityDto<TPrimaryKey>
    {
        protected DeepBridgeAsyncCrudAppServiceBase(IRepository<TEntity, TPrimaryKey> repository) : base(repository)
        {
            LocalizationSourceName = DeepBridgeConsts.LocalizationSourceName;
        }



        [HttpGet]
        public virtual async Task<TEntityDto> Get(TGetInput input)
        {
            CheckGetPermission();

            var entity = await GetEntityByIdAsync(input.Id);
            return MapToEntityDto(entity);
        }


        [HttpGet("GetAll")]
        public virtual async Task<PagedResultDto<TEntityDto>> GetAll(TGetAllInput input)
        {
            CheckGetAllPermission();

            var query = CreateFilteredQuery(input);
            query = ApplyIncludeForAll(query);

            var totalCount = await query.CountAsync();

            query = ApplySorting(query, input);
            query = ApplyPaging(query, input);

            var entities = await query.ToListAsync();

            return new PagedResultDto<TEntityDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }



        [HttpGet("GetAllForList")]
        public virtual async Task<PagedResultDto<TListEntityDto>> GetAllForList(TGetAllInput input)
        {
            CheckGetAllPermission();

            var query = CreateFilteredQuery(input);
            query = ApplyIncludeForList(query);

            var totalCount = await query.CountAsync();

            query = ApplySorting(query, input);
            query = ApplyPaging(query, input);

            var entities = await query.ToListAsync();

            return new PagedResultDto<TListEntityDto>(
                totalCount,
                entities.Select(o => ObjectMapper.Map<TListEntityDto>(o)).ToList()
            );
        }

        [HttpGet("GetAllForSelect")]
        public virtual async Task<PagedResultDto<TSelectEntityDto>> GetAllForSelect(TGetAllInput input)
        {
            CheckGetAllPermission();

            var query = CreateFilteredQuery(input);
            query = ApplyIncludeForSelect(query);

            var totalCount = await query.CountAsync();

            query = ApplySorting(query, input);
            query = ApplyPaging(query, input);

            var entities = await query.ToListAsync();

            return new PagedResultDto<TSelectEntityDto>(
                totalCount,
                entities.Select(o => ObjectMapper.Map<TSelectEntityDto>(o)).ToList()
            );
        }



        /// <summary>
        /// Create a new entity
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual async Task<ActionResult<TEntityDto>> Create(TCreateInput input)
        {
            CheckCreatePermission();

            var entity = MapToEntity(input);

            await Repository.InsertAsync(entity);
            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(entity);
        }

        [HttpPut]
        public virtual async Task<ActionResult<TEntityDto>> Update(TUpdateInput input)
        {
            CheckUpdatePermission();

            var entity = await GetEntityByIdAsync(input.Id);

            MapToEntity(input, entity);
            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(entity);
        }

        [HttpDelete]
        public virtual Task DeleteAsync(TDeleteInput input)
        {
            CheckDeletePermission();

            return Repository.DeleteAsync(input.Id);
        }

        protected virtual Task<TEntity> GetEntityByIdAsync(TPrimaryKey id)
        {
            return Repository.GetAsync(id);
        }


        protected virtual IQueryable<TEntity> ApplyIncludeForSelect(IQueryable<TEntity> query)
        {
            return query;
        }


        protected virtual IQueryable<TEntity> ApplyIncludeForList(IQueryable<TEntity> query)
        {
            return query;
        }

        protected virtual IQueryable<TEntity> ApplyIncludeForAll(IQueryable<TEntity> query)
        {
            return query;
        }

    }
}
