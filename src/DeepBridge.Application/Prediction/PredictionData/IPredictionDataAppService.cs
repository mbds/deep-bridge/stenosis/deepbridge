﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DeepBridge.Prediction.PredictionData.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeepBridge.Prediction.PredictionData
{
    public interface IPredictionDataAppService : IApplicationService, IDeepBridgeAsyncCrudAppServiceBase<PredictionDataDto, PredictionDataForListDto, PredictionDataForSelectDto, int, EntityDto, GetAllPredictionDataRequestDto, PredictionDataDto, PredictionDataDto, EntityDto>
    {
    }
}
