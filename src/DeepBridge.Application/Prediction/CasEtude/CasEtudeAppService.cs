﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using DeepBridge.Prediction.CasEtude;
using DeepBridge.Prediction.Dto;
using Microsoft.AspNetCore.Mvc;

namespace DeepBridge.Prediction
{

    [Route("api/v1/CasEtude")]
    public class CasEtudeAppService : DeepBridgeAsyncCrudAppServiceBase<Predictions.CasEtude, CasEtudeDto, CasEtudeForListDto, CasEtudeForSelectDto, int, EntityDto, GetAllCasEtudeRequestDto, CasEtudeDto, CasEtudeDto, EntityDto>, ICasEtudeAppService
    {
        public CasEtudeAppService(IRepository<Predictions.CasEtude, int> repository) : base(repository)
        {
        }
    }
}
