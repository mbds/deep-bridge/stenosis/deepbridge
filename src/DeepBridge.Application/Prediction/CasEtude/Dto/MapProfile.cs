﻿using AutoMapper;
using DeepBridge.Predictions;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeepBridge.Prediction.Dto
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            //Display oriented mappings
            CreateMap<Predictions.CasEtude, CasEtudeDto>();
            CreateMap<Predictions.CasEtude, CasEtudeForListDto>();
            CreateMap<Predictions.CasEtude, CasEtudeForSelectDto>();


            //Edit oriented mappings
            //TIP Never map child collections, it should be done manually to decide wich are updated, deleted or created
            CreateMap<CasEtudeDto, Predictions.CasEtude>();
            CreateMap<CasEtudeForListDto, Predictions.CasEtude>();
            CreateMap<CasEtudeForSelectDto, Predictions.CasEtude>();
        }
    }
}
