﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeepBridge.Prediction.Prediction.Dto
{
    public class PredictionDto : EntityDto<int>
    {
        public string Link { get; set; }
        public string Type { get; set; }
        public int PredictionDataId { get; set; }
    }
}
