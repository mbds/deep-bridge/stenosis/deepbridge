﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DeepBridge.Prediction.Model.Dto;

namespace DeepBridge.Prediction.Model
{
    public interface IModelAppService : IApplicationService, IDeepBridgeAsyncCrudAppServiceBase<ModelDto, ModelForListDto, ModelForSelectDto, int, EntityDto, GetAllModelRequestDto, ModelDto, ModelDto, EntityDto>
    {
    }
}
