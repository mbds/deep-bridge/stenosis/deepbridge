﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeepBridge.Prediction.Model.Dto
{
    public class ModelDto : EntityDto<int>
    {
        public string Link { get; set; }
        public string Task { get; set; }
        public int ImResize { get; set; }
        public int CasEtudeId { get; set; }
        public bool Aug { get; set; }
        public bool Normalize { get; set; }
        public int TypeDataId { get; set; }
        public int TypeSegmentationId { get; set; }
        public int TypeClassificationId { get; set; }
        public int Batch { get; set; }
        public int Epoch { get; set; }
        public int Rate { get; set; }
        public string Optimizeur { get; set; }
    }
}
