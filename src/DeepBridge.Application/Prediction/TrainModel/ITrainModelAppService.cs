﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DeepBridge.Prediction.TrainModel.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeepBridge.Prediction.TrainModel
{
    public interface ITrainModelAppService : IApplicationService, IDeepBridgeAsyncCrudAppServiceBase<TrainModelDto, TrainModelForListDto, TrainModelForSelectDto, int, EntityDto, GetAllTrainModelRequestDto, TrainModelDto, TrainModelDto, EntityDto>
    {
    }
}
