﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using DeepBridge.Prediction.TrainModel.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeepBridge.Prediction.TrainModel
{
    [Route("api/v1/TrainModel")]
    public class TrainModelAppService : DeepBridgeAsyncCrudAppServiceBase<Predictions.TrainModel, TrainModelDto, TrainModelForListDto, TrainModelForSelectDto, int, EntityDto, GetAllTrainModelRequestDto, TrainModelDto, TrainModelDto, EntityDto>, ITrainModelAppService
    {
        public TrainModelAppService(IRepository<Predictions.TrainModel, int> repository) : base(repository)
        {
        }
    }
}
