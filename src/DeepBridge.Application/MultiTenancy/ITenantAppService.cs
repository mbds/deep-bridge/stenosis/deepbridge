﻿using Abp.Application.Services;
using DeepBridge.MultiTenancy.Dto;

namespace DeepBridge.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

