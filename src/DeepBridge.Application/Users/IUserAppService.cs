using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DeepBridge.Roles.Dto;
using DeepBridge.Users.Dto;

namespace DeepBridge.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
