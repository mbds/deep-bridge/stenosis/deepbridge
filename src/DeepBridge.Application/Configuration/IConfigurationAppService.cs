﻿using System.Threading.Tasks;
using DeepBridge.Configuration.Dto;

namespace DeepBridge.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
